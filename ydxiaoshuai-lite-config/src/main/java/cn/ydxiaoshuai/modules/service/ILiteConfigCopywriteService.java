package cn.ydxiaoshuai.modules.service;

import cn.ydxiaoshuai.modules.entity.LiteConfigCopywrite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 刷新文案
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ILiteConfigCopywriteService extends IService<LiteConfigCopywrite> {

}

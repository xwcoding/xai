package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className GarbageBean
 * @Description 垃圾分类查询返回页面的对象
 * @Date 2020年4月26日
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GarbageBean extends BaseBean{
    //具体返回的内容
    private Data data;
    @lombok.Data
    public static class Data{
        //垃圾名称
        private String garbage_name;
        //垃圾分类
        private String cate_name;
        //城市id
        private String city_id;
        //城市名称
        private String city_name;
        //备注描述
        private String ps;
        //识别置信度，可以用来衡量识别结果，该值越大越好，建议采用值为0.7以上的结果
        private double confidence;
    }
    public GarbageBean success(String msg, String msg_zh, Data data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public GarbageBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public GarbageBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}

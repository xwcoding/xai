package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className FaceBoothBean
 * @Description 面部分析
 * @Date 2020/9/18-11:21
 **/
@NoArgsConstructor
@Data
public class FaceBoothBean {

    /**
     * extData : {"template_name":"atom/faceBooth","template_id":"G066_1"}
     * commonData : {"feRoot":"https://mms-static.cdn.bcebos.com/graph/graphfe/static","os":"ios","sidList":"10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001","graphEnv":"wise","nativeSdk":false,"sf":0,"isNaView":0,"isHalfWap":0}
     * tplData : {"title":"小度眼中的\u201c你\u201d","desc":"点击查看五官介绍和相关推荐。","faceList":[{"detect":{"top":205,"height":181,"left":98,"width":180},"faceShape":"方形脸","shapeId":0,"gender":0,"age":23,"beauty":71.36,"originDetect":[[99,383],[99,200],[280,201],[280,384]],"largeDetect":[[0,502],[0,69],[376,69],[376,502]],"xhcOriginDetect":[[0,502],[0,69],[376,69],[376,502]],"rect":[0,69,376,433],"faceShapeInfo":{"title":"方形脸","desc":"又称国字脸，此种脸型轮廓分明，下巴线条趋缓较平。此类脸型的女性意志坚强，有敏锐观察力，工作热诚有责任心","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","searchUrl":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","img":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e696b9e5bda2e884b8.22596fef.jpg","shapeTypes":[{"name":"脸型运势","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#luck"},{"name":"发型搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#hairstyle"},{"name":"服装搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#wear"},{"name":"化妆技巧","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#makeup"}],"othertypes":[{"name":"圆形脸","url":"http://graph.baidu.com/view/face?shapeId=1&gender=0"},{"name":"梨形脸","url":"http://graph.baidu.com/view/face?shapeId=2&gender=0"},{"name":"鹅蛋脸","url":"http://graph.baidu.com/view/face?shapeId=3&gender=0"},{"name":"瓜子脸","url":"http://graph.baidu.com/view/face?shapeId=4&gender=0"}]},"faceSummary":["天然呆萌","有点像方形脸"],"yaw":-2.45,"roll":-4.22,"pitch":15.35,"faceAreaRatio":0.16104317222425,"halfBaked":0,"glass":0,"simiStar":{"starList":[]},"luck":{"title":"脸型运势","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸è\u0084¸å\u009e\u008bè¿\u0090å\u008a¿","desc":"方脸女性有较强的信服力和管理能力，在事业上有晋升运势，往往会取得较大成就；在感情上，容易遭遇桃花劫，要看准真感情，通常情感婚姻收获较大的月份是阴历8月和12月"},"star":{"title":"同脸型明星","list":[{"name":"赵薇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITCBWAYZInC9UsRZmA3BCPCZtRbhIhq9UPG0kVMZWBCBUnCBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBIC.jpg","url":"http://graph.baidu.com/wordsearch?keyword=èµµè\u0096\u0087"},{"name":"李宇春","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IAI0UiGBmBW9UTGBCC3ZIsI0QRLZRPi9UZL0mlCZkCa0CPGZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVquWh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=æ\u009d\u008eå®\u0087æ\u0098¥"},{"name":"舒淇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITQ0UVahInQ9U0CZRA3BIBGZaRSZI8Y9UCw0mkM0U8MBWniZpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqZIh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=è\u0088\u0092æ·\u0087"},{"name":"孙俪","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZRhkZbuU0i9UBGuU83hIiWBcRIhIii9VhaBWsRZUPQBUXCu1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBmj.jpg","url":"http://graph.baidu.com/wordsearch?keyword=å­\u0099ä¿ª"}]},"hairstyle":{"title":"发型搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6R-WhmnihIjb9UXQZRs30kBQBQRLBCCq9U0G0UjRhUkaZUjwZdRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgauUV.jpg","title":"方脸女生适合哪些发型","desc":"不同的脸型适合不同的发型，那对于方形脸的MM们该如何选择发型呢？方形脸只要巧妙的搭配发型，也能够让你脸型变得小巧可爱哦，下面就简单的介绍下关于方形脸适合的发型吧，希望能够帮助到你们。","url":"http://jingyan.baidu.com/article/ab0b5630cfedf2c15afa7d9d.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IjMBWkYuUsI9UnGuWC3ZCP-ucRLZkAq9UniuUswhUi-0CAS0dRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZUj.jpg","title":"方脸MM适合什么发型，7款发型任你选","desc":"方脸姑娘留什么发型最秀气减龄？看欧美女明星就行！腮帮子较宽，下巴不够尖的问题在各位欧美女明星那里完全不是问题，找对合适的发型，立刻让你的大方脸减龄十岁呢，赶紧来看欧美方脸女明星是咋变方脸为神奇的吧！","url":"http://jingyan.baidu.com/article/c35dbcb02a630d8916fcbc98.html"},{"img":null,"title":"方脸女神支招最美修颜发型","desc":"纵观娱乐圈，女明星们似乎都是一致的小脸美女，要给大家看看娱乐圈那些方脸美女，她们一样很惊艳！","url":"http://lady.people.com.cn/n/2015/0804/c1014-27405655.html"}]},"wear":{"title":"服饰搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RXWZCAI0ksb9UVwBCZ3hkXGucRq0Csb9UZw0kCS0UAqhmVqB1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRBWg.jpg","title":"女生方脸适合什么帽子","desc":"方脸型适合什么帽子？今天和大家一起看看方脸适合什么帽子吧。世界上有千千万万种脸型，每一种脸型有它自己独特的美，也有它自己独特的好处，那么假如你是方形脸，又想要带帽子的话，那么这时候可能会有点迷茫不知道","url":"http://dress.yxlady.com/201504/707591.shtml"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6I-WBRPGBUkS9UAL0UV30UgM0cSihRha9VVahVlGBVZqBmjL0dRenFvkrUgquWgquQXRfFSvrUCRBm8wuUhLuWV.jpg","title":"网购衣服党必看秘籍之宽脸/方脸/国字脸如何选购衣服","desc":"\"在生活当中，每个平凡的女生都是女神的潜力股，只是因为不恰当的装扮而掩盖自己的美丽。脸界霸王龙\u2014\u2014存在感超强的的宽脸、国字脸。该怎样穿才能使自己的腮帮子不那么明显。\"","url":"https://m.douban.com/note/518637096/"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZqBUkwZIsa9VZbuUV3BWkRZQRSBI8b9UX-hUgwZRAqhmgYZwRenFvkrUgquWgquQXRfFSvrUCRBm8YZUkRZIZ.jpg","title":"方形脸挑墨镜技巧","desc":"方型脸不建议选择太圆的，太圆会觉得角很明显，但是又不能选择太方的，太方会凸显你的线条。最好选择大的、但角度有点圆弧的。方型脸的侧面也会有点宽，如果选太细的镜腿，会显得腮部更明显，要避免这点，可以选脚宽","url":"http://fashion.sina.com.cn/s/ce/2015-05-19/0811/doc-icczmvup1895478-p3.shtml"}]},"makeup":{"title":"化妆技巧","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ICYBmAa0kZI9UkIuWV3BmhLZQRSBC0Q9UBCBmXWZWAaBRARBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZIg.jpg","title":"女生方脸化妆技巧","desc":"脸小的美眉给人的感觉，总是精致，像精灵一样惹人喜欢，然而方脸的美眉们也不用羡慕别人拥有的精致的小脸蛋了，今天教你们学习方脸的化妆技巧，打造属于自己的小脸。","url":"http://wenku.baidu.com/link?url=4HLJyCgGyaVETo7jJc34wUAN89LbSMYiineDcbxM-9DkltO9j-d21fziuJwCrk7QxKaKjkgqjfiC7ojFmDqZiCX8pvisSVP6OtgUgppdKAO"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RBi0UjSZRP-9VZb0WC3BUVwBcRYBI-G9UnWZU8R0mn-hVX-u1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZWA.jpg","title":"舒淇方脸淡妆同样女神范","desc":"精致的锥子脸是定义美女的标准，轮廓分明的方脸虽然看起来骨架比锥子脸大，可是妆容得宜同样吸睛美丽，圈中不乏方脸女神，舒淇、赵薇、高圆圆等都是方脸女神的代表。","url":"http://lady.163.com/13/1021/11/9BN5TKKD00264MHO.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXGuUjLZWT-9U--Zm83hInWBtS-BkCR9UX-ZIiCBWP-0WkqZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZUA.jpg","title":"怎样化方脸彩妆","desc":"很多MM都认为方脸化彩妆很难看，其实不然，看了米粒彩妆达人的《教你如何化彩妆之方脸彩妆》后，你就能轻松的搞定化方脸彩妆，\u201c妆\u201d出属于自己的美丽。","url":"http://jingyan.baidu.com/album/ab69b270e9d4642ca7189fc6.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXihmPihkZL9U8q0UV3uUn-ucRMBkhb9VPiZIiGhmsYhIgIBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZmh.jpg","title":"方脸如何化妆","desc":"方脸的人我们应该是见过很多的，那么给我印象最大的就是脸很方正，而且方脸的人看起来脸是很大的，如果我们从正面看的话，总是觉的方面并不是那么的好看的，因此对于我们方脸的人来，要如何化妆才好呢？","url":"http://jingyan.baidu.com/article/15622f24485b28fdfdbea57f.html"}]},"hair":[],"organs":[[{"name":"标准眉","faceInfo":{"faceOrganInfo":{"title":"眉毛简介","name":"眉毛","desc":"又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]},"faceLevel":91,"faceFortune":83},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=brow&brow=æ \u0087å\u0087\u0086ç\u009c\u0089&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[119.70999908447,210.81500244141],"y":210.81500244141},{"name":"圆眼","faceInfo":{"faceOrganInfo":{"title":"眼形简介","name":"眼形","desc":"又称铜铃眼，眼睛圆的像铜铃，眼角边缘呈圆弧形，是最具福气的眼型了，大大圆圆的眼睛似乎里面藏了小精灵，聪明机灵、颜值爆棚。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e9939ce99383e79cbc.b98c60ac.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"三分钟就能搞定的日常眼妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232922?from=baidu&recommend=1","duration":"2:22","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1049.jpg","desc":"这个眼妆很日常，大约三分钟就能搞定喔"},{"imgType":"video","title":"單眼皮的基本妝容分享","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231650?from=baidu&recommend=1","duration":"5:47","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1051.jpg","desc":"分享单眼皮的基本妆容分享"},{"imgType":"video","title":"周末眼妆look\u2014\u2014红棕色的初秋","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227421?from=baidu&recommend=1","duration":"2:3","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/9695F8C75E4B249C.jpg-big2x.jpg ","desc":"日常眼妆分享"},{"imgType":"image","title":"火爆ins的亮片妆容","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180737?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1023.jpg","desc":"谁还不是个小仙女儿～现在超火爆的亮片妆容再搭配上双丸子头，可爱哟～"},{"imgType":"image","title":"你画出自己想要的眼影效果了吗？","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/176052?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502875845322c293dcb-small3x.jpg","desc":"给宝贝们分享一个眼影的画法"}]},"faceLevel":92,"faceFortune":96},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=eye&eye=é\u0093\u009cé\u0093\u0083ç\u009c¼&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[246.69499969482,236.5],"y":236.5}],[{"name":"标准鼻","faceInfo":{"faceOrganInfo":{"title":"鼻形简介","name":"鼻形","desc":"又称希腊鼻，此种鼻形鼻梁挺直、鼻头尖圆、鼻梁长度与鼻翼宽度的比例适当，百搭任何脸型，给人感觉高雅有气质、福气与颜值同在。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5b88ce8858ae9bcbb.d95051d6.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"鼻影大法好|两种不同产品的画法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/224191?from=baidu&recommend=1","duration":"1:49","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1072.jpg","desc":"和塌鼻子抗争多年的我终于研究出一套适合自己的画法希望可以帮助到你们！"},{"imgType":"video","title":"掌握鼻影修容术｜拯救烂大街的假鼻子","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/222378?from=baidu&recommend=1","duration":"2:52","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1FA01B8B44F8E457-big2x.jpg ","desc":"生活里大家的鼻子总归有不完美的地方，所以这节课我们就来讲鼻影啦！"},{"imgType":"video","title":"不打玻尿酸也有高鼻梁？刷对鼻影就好！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/221613?from=baidu&recommend=1","duration":"4:46","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1020.jpg","desc":"希望大家都可以学会，毕竟我觉得鼻子太重要了，谁让我没有高鼻梁呢！"},{"imgType":"video","title":"塌鼻子蒜头鼻怎么破？鼻影美图帮你忙","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/216075?from=baidu&recommend=1","duration":"4:37","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/FE06625A34D31801-big2x.jpg ","desc":"跟大家分享一下我自己平时是怎么画鼻子的，鼻影再修饰轮廓脸型来说是很重要的！ "}]},"faceLevel":91,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=nose&nose=å¸\u008cè\u0085\u008aé¼»&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[186.97999572754,298.13000488281],"y":298.13000488281},{"name":"白皙色","faceInfo":{"faceOrganInfo":{"title":"肤色简介","name":"肤色","desc":"又称亮白色，皮肤白皙细腻，闪耀亮白，给人一尘不染的感觉，是所有人都羡慕的肤色，不但颜值很高，就连化妆都能省去很多步骤。","imgUrl":"https://mms-graph.cdn.bcebos.com/face/faceOrgan/baixise.png"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"image","title":"包学包会的修容课堂 脸大也不怕了！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/161393?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1027.jpg","desc":"修容真正的意义在于，让轮廓更明显，五官更加立体"},{"imgType":"video","title":"【Rainie】第一次肤色修正？惊悚慎点哈","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/209573?from=baidu&recommend=1","duration":"7:11","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1021.jpg","desc":"不管你是黄皮白皮还是黑皮，肤色修正都相当重要！"},{"imgType":"video","title":"如何调整面部肤色不均","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/129454?from=baidu&recommend=1","duration":"2:18","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1022.jpg","desc":"如何调整面部肤色不均 ，这里有你必须了解的东西"},{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "}]},"faceLevel":97,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=skin_color&skin_color=äº®ç\u0099½è\u0089²_226_177_162&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[240.83999633789,324.82499694824],"y":324.82499694824}],[{"name":"瓜子脸","faceInfo":{"faceOrganInfo":{"title":"脸型简介","name":"脸型","desc":"又称心形脸，轮廓接近T形，下巴前翘明显。脸型标致，颜值爆表，此脸型的人善于创新，易得贵人赏识，所以是福气也很棒哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e5bf83e5bda2e884b8.730636d5.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"圆脸也适用的超自然日常妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/192366?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/DB7EF5AC725F690E.jpg-small3x.jpg","desc":"分享我觉得好看实用的技巧和妆容"},{"imgType":"video","title":"如何把大方脸化成欧式混血妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/230737?from=baidu&recommend=1","duration":"4:35","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1037.jpg ","desc":"们的模特是方脸妹子 一个妆容就能拯救"},{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"不用整容，大饼脸变小V脸！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227098?from=baidu&recommend=1","duration":"3:25","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/B5865F8726F97EBC-big2x.jpg ","desc":"什么，利用化妆修容可以改变脸型？"},{"imgType":"video","title":"四种不同脸型整容级修容法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/223793?from=baidu&recommend=1","duration":"3:10","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502288737103_Ji7HMZEW4D.png-big2x.jpg ","desc":"修容这个步骤不仅仅是变小脸这么简单，而是重塑五官的轮廓，让它看起来更加立体更自然"}]},"faceLevel":98,"faceFortune":94},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=face_shape&face_shape=ç\u0093\u009cå­\u0090è\u0084¸&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[175.4700012207,363.97999572754],"y":363.97999572754},{"name":"仰月唇","faceInfo":{"faceOrganInfo":{"title":"唇形简介","name":"唇形","desc":"又称弓口唇，上唇如弯弓一样棱角分明，嘴角上翘而饱满，是很有福气的唇形了，给人感觉气质性感而有个性，因此为颜值加分不少。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5bc93e58fa3e59487.c5a4a984.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"复古风 气质红唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232999?from=baidu&recommend=1","duration":"4:58","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1106.jpg","desc":"拯救单眼皮系列，复古感的气质红唇妆容！"},{"imgType":"video","title":"口红涂抹挑战，做百变女魔王","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232912?from=baidu&recommend=1","duration":"3:8","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1096.jpg","desc":"口红涂抹千姿百态！绽放不同的色彩效果～薄涂厚涂咬唇都在大小不一样的自己"},{"imgType":"video","title":"3种基础唇妆，帮你颜值飙升","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232477?from=baidu&recommend=1","duration":"1:59","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1098.jpg","desc":"画好唇妆，飙升的不仅仅是颜值，还有气质！"},{"imgType":"video","title":"红丝绒唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231746?from=baidu&recommend=1","duration":"1:13","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1091.jpg","desc":"暖暖红丝绒唇妆，超有质感"},{"imgType":"video","title":"教你化个布灵布灵的时尚玻璃唇！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231532?from=baidu&recommend=1","duration":"1:45","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1092.jpg","desc":"咬唇妆out了！教你化个布灵布灵的时尚玻璃唇！"}]},"faceLevel":92,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=mouth&mouth=å¼\u0093å\u008f£å\u0094\u0087&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[188.97499847412,334.96000671387],"y":334.96000671387}]]}],"draw":1,"titleUrl":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=face_shape&face_shape=ç\u0093\u009cå­\u0090è\u0084¸&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","imgUrl":"http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1","share":{"shareTitle":"我在百度体验AI五官识别！","shareText":"百度AI脸部信息识别，识面相、测颜值，为你匹配最佳推荐！","shareUrl":"http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e","iconUrl":"http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1","wxShareToken":{"appid":"wxff7da0c6a7491bca","timestamp":1600396177,"nonceStr":"a4d90f1d-5bb9","signature":"dc07893a2bd369336de7da932a4b0226d2bd4984","ticket":"kgt8ON7yVITDhtdwci0qeaM9lZK2sNS5_YoZQKbype1kDrJoUls4ZFT-SjUHYWLgE3_rBgud9Lq4H_W5gtab3A","url":"http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e"}},"editImgUrl":"http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1"}
     */

    private ExtDataBean extData;
    private CommonDataBean commonData;
    private TplDataBean tplData;

    @NoArgsConstructor
    @Data
    public static class ExtDataBean {
        /**
         * template_name : atom/faceBooth
         * template_id : G066_1
         */

        private String template_name;
        private String template_id;
    }

    @NoArgsConstructor
    @Data
    public static class CommonDataBean {
        /**
         * feRoot : https://mms-static.cdn.bcebos.com/graph/graphfe/static
         * os : ios
         * sidList : 10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001
         * graphEnv : wise
         * nativeSdk : false
         * sf : 0
         * isNaView : 0
         * isHalfWap : 0
         */

        private String feRoot;
        private String os;
        private String sidList;
        private String graphEnv;
        private boolean nativeSdk;
        private int sf;
        private int isNaView;
        private int isHalfWap;
    }

    @NoArgsConstructor
    @Data
    public static class TplDataBean {
        /**
         * title : 小度眼中的“你”
         * desc : 点击查看五官介绍和相关推荐。
         * faceList : [{"detect":{"top":205,"height":181,"left":98,"width":180},"faceShape":"方形脸","shapeId":0,"gender":0,"age":23,"beauty":71.36,"originDetect":[[99,383],[99,200],[280,201],[280,384]],"largeDetect":[[0,502],[0,69],[376,69],[376,502]],"xhcOriginDetect":[[0,502],[0,69],[376,69],[376,502]],"rect":[0,69,376,433],"faceShapeInfo":{"title":"方形脸","desc":"又称国字脸，此种脸型轮廓分明，下巴线条趋缓较平。此类脸型的女性意志坚强，有敏锐观察力，工作热诚有责任心","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","searchUrl":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","img":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e696b9e5bda2e884b8.22596fef.jpg","shapeTypes":[{"name":"脸型运势","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#luck"},{"name":"发型搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#hairstyle"},{"name":"服装搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#wear"},{"name":"化妆技巧","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#makeup"}],"othertypes":[{"name":"圆形脸","url":"http://graph.baidu.com/view/face?shapeId=1&gender=0"},{"name":"梨形脸","url":"http://graph.baidu.com/view/face?shapeId=2&gender=0"},{"name":"鹅蛋脸","url":"http://graph.baidu.com/view/face?shapeId=3&gender=0"},{"name":"瓜子脸","url":"http://graph.baidu.com/view/face?shapeId=4&gender=0"}]},"faceSummary":["天然呆萌","有点像方形脸"],"yaw":-2.45,"roll":-4.22,"pitch":15.35,"faceAreaRatio":0.16104317222425,"halfBaked":0,"glass":0,"simiStar":{"starList":[]},"luck":{"title":"脸型运势","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸è\u0084¸å\u009e\u008bè¿\u0090å\u008a¿","desc":"方脸女性有较强的信服力和管理能力，在事业上有晋升运势，往往会取得较大成就；在感情上，容易遭遇桃花劫，要看准真感情，通常情感婚姻收获较大的月份是阴历8月和12月"},"star":{"title":"同脸型明星","list":[{"name":"赵薇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITCBWAYZInC9UsRZmA3BCPCZtRbhIhq9UPG0kVMZWBCBUnCBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBIC.jpg","url":"http://graph.baidu.com/wordsearch?keyword=èµµè\u0096\u0087"},{"name":"李宇春","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IAI0UiGBmBW9UTGBCC3ZIsI0QRLZRPi9UZL0mlCZkCa0CPGZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVquWh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=æ\u009d\u008eå®\u0087æ\u0098¥"},{"name":"舒淇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITQ0UVahInQ9U0CZRA3BIBGZaRSZI8Y9UCw0mkM0U8MBWniZpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqZIh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=è\u0088\u0092æ·\u0087"},{"name":"孙俪","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZRhkZbuU0i9UBGuU83hIiWBcRIhIii9VhaBWsRZUPQBUXCu1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBmj.jpg","url":"http://graph.baidu.com/wordsearch?keyword=å­\u0099ä¿ª"}]},"hairstyle":{"title":"发型搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6R-WhmnihIjb9UXQZRs30kBQBQRLBCCq9U0G0UjRhUkaZUjwZdRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgauUV.jpg","title":"方脸女生适合哪些发型","desc":"不同的脸型适合不同的发型，那对于方形脸的MM们该如何选择发型呢？方形脸只要巧妙的搭配发型，也能够让你脸型变得小巧可爱哦，下面就简单的介绍下关于方形脸适合的发型吧，希望能够帮助到你们。","url":"http://jingyan.baidu.com/article/ab0b5630cfedf2c15afa7d9d.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IjMBWkYuUsI9UnGuWC3ZCP-ucRLZkAq9UniuUswhUi-0CAS0dRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZUj.jpg","title":"方脸MM适合什么发型，7款发型任你选","desc":"方脸姑娘留什么发型最秀气减龄？看欧美女明星就行！腮帮子较宽，下巴不够尖的问题在各位欧美女明星那里完全不是问题，找对合适的发型，立刻让你的大方脸减龄十岁呢，赶紧来看欧美方脸女明星是咋变方脸为神奇的吧！","url":"http://jingyan.baidu.com/article/c35dbcb02a630d8916fcbc98.html"},{"img":null,"title":"方脸女神支招最美修颜发型","desc":"纵观娱乐圈，女明星们似乎都是一致的小脸美女，要给大家看看娱乐圈那些方脸美女，她们一样很惊艳！","url":"http://lady.people.com.cn/n/2015/0804/c1014-27405655.html"}]},"wear":{"title":"服饰搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RXWZCAI0ksb9UVwBCZ3hkXGucRq0Csb9UZw0kCS0UAqhmVqB1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRBWg.jpg","title":"女生方脸适合什么帽子","desc":"方脸型适合什么帽子？今天和大家一起看看方脸适合什么帽子吧。世界上有千千万万种脸型，每一种脸型有它自己独特的美，也有它自己独特的好处，那么假如你是方形脸，又想要带帽子的话，那么这时候可能会有点迷茫不知道","url":"http://dress.yxlady.com/201504/707591.shtml"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6I-WBRPGBUkS9UAL0UV30UgM0cSihRha9VVahVlGBVZqBmjL0dRenFvkrUgquWgquQXRfFSvrUCRBm8wuUhLuWV.jpg","title":"网购衣服党必看秘籍之宽脸/方脸/国字脸如何选购衣服","desc":"\"在生活当中，每个平凡的女生都是女神的潜力股，只是因为不恰当的装扮而掩盖自己的美丽。脸界霸王龙\u2014\u2014存在感超强的的宽脸、国字脸。该怎样穿才能使自己的腮帮子不那么明显。\"","url":"https://m.douban.com/note/518637096/"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZqBUkwZIsa9VZbuUV3BWkRZQRSBI8b9UX-hUgwZRAqhmgYZwRenFvkrUgquWgquQXRfFSvrUCRBm8YZUkRZIZ.jpg","title":"方形脸挑墨镜技巧","desc":"方型脸不建议选择太圆的，太圆会觉得角很明显，但是又不能选择太方的，太方会凸显你的线条。最好选择大的、但角度有点圆弧的。方型脸的侧面也会有点宽，如果选太细的镜腿，会显得腮部更明显，要避免这点，可以选脚宽","url":"http://fashion.sina.com.cn/s/ce/2015-05-19/0811/doc-icczmvup1895478-p3.shtml"}]},"makeup":{"title":"化妆技巧","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ICYBmAa0kZI9UkIuWV3BmhLZQRSBC0Q9UBCBmXWZWAaBRARBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZIg.jpg","title":"女生方脸化妆技巧","desc":"脸小的美眉给人的感觉，总是精致，像精灵一样惹人喜欢，然而方脸的美眉们也不用羡慕别人拥有的精致的小脸蛋了，今天教你们学习方脸的化妆技巧，打造属于自己的小脸。","url":"http://wenku.baidu.com/link?url=4HLJyCgGyaVETo7jJc34wUAN89LbSMYiineDcbxM-9DkltO9j-d21fziuJwCrk7QxKaKjkgqjfiC7ojFmDqZiCX8pvisSVP6OtgUgppdKAO"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RBi0UjSZRP-9VZb0WC3BUVwBcRYBI-G9UnWZU8R0mn-hVX-u1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZWA.jpg","title":"舒淇方脸淡妆同样女神范","desc":"精致的锥子脸是定义美女的标准，轮廓分明的方脸虽然看起来骨架比锥子脸大，可是妆容得宜同样吸睛美丽，圈中不乏方脸女神，舒淇、赵薇、高圆圆等都是方脸女神的代表。","url":"http://lady.163.com/13/1021/11/9BN5TKKD00264MHO.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXGuUjLZWT-9U--Zm83hInWBtS-BkCR9UX-ZIiCBWP-0WkqZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZUA.jpg","title":"怎样化方脸彩妆","desc":"很多MM都认为方脸化彩妆很难看，其实不然，看了米粒彩妆达人的《教你如何化彩妆之方脸彩妆》后，你就能轻松的搞定化方脸彩妆，\u201c妆\u201d出属于自己的美丽。","url":"http://jingyan.baidu.com/album/ab69b270e9d4642ca7189fc6.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXihmPihkZL9U8q0UV3uUn-ucRMBkhb9VPiZIiGhmsYhIgIBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZmh.jpg","title":"方脸如何化妆","desc":"方脸的人我们应该是见过很多的，那么给我印象最大的就是脸很方正，而且方脸的人看起来脸是很大的，如果我们从正面看的话，总是觉的方面并不是那么的好看的，因此对于我们方脸的人来，要如何化妆才好呢？","url":"http://jingyan.baidu.com/article/15622f24485b28fdfdbea57f.html"}]},"hair":[],"organs":[[{"name":"标准眉","faceInfo":{"faceOrganInfo":{"title":"眉毛简介","name":"眉毛","desc":"又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]},"faceLevel":91,"faceFortune":83},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=brow&brow=æ \u0087å\u0087\u0086ç\u009c\u0089&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[119.70999908447,210.81500244141],"y":210.81500244141},{"name":"圆眼","faceInfo":{"faceOrganInfo":{"title":"眼形简介","name":"眼形","desc":"又称铜铃眼，眼睛圆的像铜铃，眼角边缘呈圆弧形，是最具福气的眼型了，大大圆圆的眼睛似乎里面藏了小精灵，聪明机灵、颜值爆棚。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e9939ce99383e79cbc.b98c60ac.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"三分钟就能搞定的日常眼妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232922?from=baidu&recommend=1","duration":"2:22","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1049.jpg","desc":"这个眼妆很日常，大约三分钟就能搞定喔"},{"imgType":"video","title":"單眼皮的基本妝容分享","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231650?from=baidu&recommend=1","duration":"5:47","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1051.jpg","desc":"分享单眼皮的基本妆容分享"},{"imgType":"video","title":"周末眼妆look\u2014\u2014红棕色的初秋","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227421?from=baidu&recommend=1","duration":"2:3","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/9695F8C75E4B249C.jpg-big2x.jpg ","desc":"日常眼妆分享"},{"imgType":"image","title":"火爆ins的亮片妆容","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180737?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1023.jpg","desc":"谁还不是个小仙女儿～现在超火爆的亮片妆容再搭配上双丸子头，可爱哟～"},{"imgType":"image","title":"你画出自己想要的眼影效果了吗？","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/176052?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502875845322c293dcb-small3x.jpg","desc":"给宝贝们分享一个眼影的画法"}]},"faceLevel":92,"faceFortune":96},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=eye&eye=é\u0093\u009cé\u0093\u0083ç\u009c¼&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[246.69499969482,236.5],"y":236.5}],[{"name":"标准鼻","faceInfo":{"faceOrganInfo":{"title":"鼻形简介","name":"鼻形","desc":"又称希腊鼻，此种鼻形鼻梁挺直、鼻头尖圆、鼻梁长度与鼻翼宽度的比例适当，百搭任何脸型，给人感觉高雅有气质、福气与颜值同在。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5b88ce8858ae9bcbb.d95051d6.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"鼻影大法好|两种不同产品的画法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/224191?from=baidu&recommend=1","duration":"1:49","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1072.jpg","desc":"和塌鼻子抗争多年的我终于研究出一套适合自己的画法希望可以帮助到你们！"},{"imgType":"video","title":"掌握鼻影修容术｜拯救烂大街的假鼻子","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/222378?from=baidu&recommend=1","duration":"2:52","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1FA01B8B44F8E457-big2x.jpg ","desc":"生活里大家的鼻子总归有不完美的地方，所以这节课我们就来讲鼻影啦！"},{"imgType":"video","title":"不打玻尿酸也有高鼻梁？刷对鼻影就好！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/221613?from=baidu&recommend=1","duration":"4:46","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1020.jpg","desc":"希望大家都可以学会，毕竟我觉得鼻子太重要了，谁让我没有高鼻梁呢！"},{"imgType":"video","title":"塌鼻子蒜头鼻怎么破？鼻影美图帮你忙","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/216075?from=baidu&recommend=1","duration":"4:37","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/FE06625A34D31801-big2x.jpg ","desc":"跟大家分享一下我自己平时是怎么画鼻子的，鼻影再修饰轮廓脸型来说是很重要的！ "}]},"faceLevel":91,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=nose&nose=å¸\u008cè\u0085\u008aé¼»&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[186.97999572754,298.13000488281],"y":298.13000488281},{"name":"白皙色","faceInfo":{"faceOrganInfo":{"title":"肤色简介","name":"肤色","desc":"又称亮白色，皮肤白皙细腻，闪耀亮白，给人一尘不染的感觉，是所有人都羡慕的肤色，不但颜值很高，就连化妆都能省去很多步骤。","imgUrl":"https://mms-graph.cdn.bcebos.com/face/faceOrgan/baixise.png"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"image","title":"包学包会的修容课堂 脸大也不怕了！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/161393?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1027.jpg","desc":"修容真正的意义在于，让轮廓更明显，五官更加立体"},{"imgType":"video","title":"【Rainie】第一次肤色修正？惊悚慎点哈","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/209573?from=baidu&recommend=1","duration":"7:11","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1021.jpg","desc":"不管你是黄皮白皮还是黑皮，肤色修正都相当重要！"},{"imgType":"video","title":"如何调整面部肤色不均","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/129454?from=baidu&recommend=1","duration":"2:18","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1022.jpg","desc":"如何调整面部肤色不均 ，这里有你必须了解的东西"},{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "}]},"faceLevel":97,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=skin_color&skin_color=äº®ç\u0099½è\u0089²_226_177_162&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[240.83999633789,324.82499694824],"y":324.82499694824}],[{"name":"瓜子脸","faceInfo":{"faceOrganInfo":{"title":"脸型简介","name":"脸型","desc":"又称心形脸，轮廓接近T形，下巴前翘明显。脸型标致，颜值爆表，此脸型的人善于创新，易得贵人赏识，所以是福气也很棒哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e5bf83e5bda2e884b8.730636d5.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"圆脸也适用的超自然日常妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/192366?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/DB7EF5AC725F690E.jpg-small3x.jpg","desc":"分享我觉得好看实用的技巧和妆容"},{"imgType":"video","title":"如何把大方脸化成欧式混血妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/230737?from=baidu&recommend=1","duration":"4:35","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1037.jpg ","desc":"们的模特是方脸妹子 一个妆容就能拯救"},{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"不用整容，大饼脸变小V脸！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227098?from=baidu&recommend=1","duration":"3:25","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/B5865F8726F97EBC-big2x.jpg ","desc":"什么，利用化妆修容可以改变脸型？"},{"imgType":"video","title":"四种不同脸型整容级修容法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/223793?from=baidu&recommend=1","duration":"3:10","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502288737103_Ji7HMZEW4D.png-big2x.jpg ","desc":"修容这个步骤不仅仅是变小脸这么简单，而是重塑五官的轮廓，让它看起来更加立体更自然"}]},"faceLevel":98,"faceFortune":94},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=face_shape&face_shape=ç\u0093\u009cå­\u0090è\u0084¸&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[175.4700012207,363.97999572754],"y":363.97999572754},{"name":"仰月唇","faceInfo":{"faceOrganInfo":{"title":"唇形简介","name":"唇形","desc":"又称弓口唇，上唇如弯弓一样棱角分明，嘴角上翘而饱满，是很有福气的唇形了，给人感觉气质性感而有个性，因此为颜值加分不少。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5bc93e58fa3e59487.c5a4a984.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"复古风 气质红唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232999?from=baidu&recommend=1","duration":"4:58","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1106.jpg","desc":"拯救单眼皮系列，复古感的气质红唇妆容！"},{"imgType":"video","title":"口红涂抹挑战，做百变女魔王","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232912?from=baidu&recommend=1","duration":"3:8","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1096.jpg","desc":"口红涂抹千姿百态！绽放不同的色彩效果～薄涂厚涂咬唇都在大小不一样的自己"},{"imgType":"video","title":"3种基础唇妆，帮你颜值飙升","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232477?from=baidu&recommend=1","duration":"1:59","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1098.jpg","desc":"画好唇妆，飙升的不仅仅是颜值，还有气质！"},{"imgType":"video","title":"红丝绒唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231746?from=baidu&recommend=1","duration":"1:13","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1091.jpg","desc":"暖暖红丝绒唇妆，超有质感"},{"imgType":"video","title":"教你化个布灵布灵的时尚玻璃唇！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231532?from=baidu&recommend=1","duration":"1:45","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1092.jpg","desc":"咬唇妆out了！教你化个布灵布灵的时尚玻璃唇！"}]},"faceLevel":92,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=mouth&mouth=å¼\u0093å\u008f£å\u0094\u0087&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[188.97499847412,334.96000671387],"y":334.96000671387}]]}]
         * draw : 1
         * titleUrl : https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=face_shape&face_shape=çå­è¸&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1
         * imgUrl : http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1
         * share : {"shareTitle":"我在百度体验AI五官识别！","shareText":"百度AI脸部信息识别，识面相、测颜值，为你匹配最佳推荐！","shareUrl":"http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e","iconUrl":"http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1","wxShareToken":{"appid":"wxff7da0c6a7491bca","timestamp":1600396177,"nonceStr":"a4d90f1d-5bb9","signature":"dc07893a2bd369336de7da932a4b0226d2bd4984","ticket":"kgt8ON7yVITDhtdwci0qeaM9lZK2sNS5_YoZQKbype1kDrJoUls4ZFT-SjUHYWLgE3_rBgud9Lq4H_W5gtab3A","url":"http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e"}}
         * editImgUrl : http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1
         */

        private String title;
        private String desc;
        private int draw;
        private String titleUrl;
        private String imgUrl;
        private ShareBean share;
        private String editImgUrl;
        private List<FaceListBean> faceList;

        @NoArgsConstructor
        @Data
        public static class ShareBean {
            /**
             * shareTitle : 我在百度体验AI五官识别！
             * shareText : 百度AI脸部信息识别，识面相、测颜值，为你匹配最佳推荐！
             * shareUrl : http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e
             * iconUrl : http://graph.baidu.com/resource/226d73b7e1cf86e0cfcf601600396176.jpg?isbface=1
             * wxShareToken : {"appid":"wxff7da0c6a7491bca","timestamp":1600396177,"nonceStr":"a4d90f1d-5bb9","signature":"dc07893a2bd369336de7da932a4b0226d2bd4984","ticket":"kgt8ON7yVITDhtdwci0qeaM9lZK2sNS5_YoZQKbype1kDrJoUls4ZFT-SjUHYWLgE3_rBgud9Lq4H_W5gtab3A","url":"http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e"}
             */

            private String shareTitle;
            private String shareText;
            private String shareUrl;
            private String iconUrl;
            private WxShareTokenBean wxShareToken;

            @NoArgsConstructor
            @Data
            public static class WxShareTokenBean {
                /**
                 * appid : wxff7da0c6a7491bca
                 * timestamp : 1600396177
                 * nonceStr : a4d90f1d-5bb9
                 * signature : dc07893a2bd369336de7da932a4b0226d2bd4984
                 * ticket : kgt8ON7yVITDhtdwci0qeaM9lZK2sNS5_YoZQKbype1kDrJoUls4ZFT-SjUHYWLgE3_rBgud9Lq4H_W5gtab3A
                 * url : http://graph.baidu.com/s?sign=226d73b7e1cf86e0cfcf601600396176&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1&isshare=1&osname=baiduboxapp&isSharePage=1&f=face&user_key=d41d8cd98f00b204e9800998ecf8427e
                 */

                private String appid;
                private int timestamp;
                private String nonceStr;
                private String signature;
                private String ticket;
                private String url;
            }
        }

        @NoArgsConstructor
        @Data
        public static class FaceListBean {
            /**
             * detect : {"top":205,"height":181,"left":98,"width":180}
             * faceShape : 方形脸
             * shapeId : 0
             * gender : 0
             * age : 23
             * beauty : 71.36
             * originDetect : [[99,383],[99,200],[280,201],[280,384]]
             * largeDetect : [[0,502],[0,69],[376,69],[376,502]]
             * xhcOriginDetect : [[0,502],[0,69],[376,69],[376,502]]
             * rect : [0,69,376,433]
             * faceShapeInfo : {"title":"方形脸","desc":"又称国字脸，此种脸型轮廓分明，下巴线条趋缓较平。此类脸型的女性意志坚强，有敏锐观察力，工作热诚有责任心","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","searchUrl":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸","img":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e696b9e5bda2e884b8.22596fef.jpg","shapeTypes":[{"name":"脸型运势","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#luck"},{"name":"发型搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#hairstyle"},{"name":"服装搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#wear"},{"name":"化妆技巧","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#makeup"}],"othertypes":[{"name":"圆形脸","url":"http://graph.baidu.com/view/face?shapeId=1&gender=0"},{"name":"梨形脸","url":"http://graph.baidu.com/view/face?shapeId=2&gender=0"},{"name":"鹅蛋脸","url":"http://graph.baidu.com/view/face?shapeId=3&gender=0"},{"name":"瓜子脸","url":"http://graph.baidu.com/view/face?shapeId=4&gender=0"}]}
             * faceSummary : ["天然呆萌","有点像方形脸"]
             * yaw : -2.45
             * roll : -4.22
             * pitch : 15.35
             * faceAreaRatio : 0.16104317222425
             * halfBaked : 0
             * glass : 0
             * simiStar : {"starList":[]}
             * luck : {"title":"脸型运势","url":"https://www.baidu.com/s?ie=UTF-8&wd=æ\u0096¹å½¢è\u0084¸è\u0084¸å\u009e\u008bè¿\u0090å\u008a¿","desc":"方脸女性有较强的信服力和管理能力，在事业上有晋升运势，往往会取得较大成就；在感情上，容易遭遇桃花劫，要看准真感情，通常情感婚姻收获较大的月份是阴历8月和12月"}
             * star : {"title":"同脸型明星","list":[{"name":"赵薇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITCBWAYZInC9UsRZmA3BCPCZtRbhIhq9UPG0kVMZWBCBUnCBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBIC.jpg","url":"http://graph.baidu.com/wordsearch?keyword=èµµè\u0096\u0087"},{"name":"李宇春","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IAI0UiGBmBW9UTGBCC3ZIsI0QRLZRPi9UZL0mlCZkCa0CPGZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVquWh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=æ\u009d\u008eå®\u0087æ\u0098¥"},{"name":"舒淇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITQ0UVahInQ9U0CZRA3BIBGZaRSZI8Y9UCw0mkM0U8MBWniZpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqZIh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=è\u0088\u0092æ·\u0087"},{"name":"孙俪","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZRhkZbuU0i9UBGuU83hIiWBcRIhIii9VhaBWsRZUPQBUXCu1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBmj.jpg","url":"http://graph.baidu.com/wordsearch?keyword=å­\u0099ä¿ª"}]}
             * hairstyle : {"title":"发型搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6R-WhmnihIjb9UXQZRs30kBQBQRLBCCq9U0G0UjRhUkaZUjwZdRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgauUV.jpg","title":"方脸女生适合哪些发型","desc":"不同的脸型适合不同的发型，那对于方形脸的MM们该如何选择发型呢？方形脸只要巧妙的搭配发型，也能够让你脸型变得小巧可爱哦，下面就简单的介绍下关于方形脸适合的发型吧，希望能够帮助到你们。","url":"http://jingyan.baidu.com/article/ab0b5630cfedf2c15afa7d9d.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IjMBWkYuUsI9UnGuWC3ZCP-ucRLZkAq9UniuUswhUi-0CAS0dRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZUj.jpg","title":"方脸MM适合什么发型，7款发型任你选","desc":"方脸姑娘留什么发型最秀气减龄？看欧美女明星就行！腮帮子较宽，下巴不够尖的问题在各位欧美女明星那里完全不是问题，找对合适的发型，立刻让你的大方脸减龄十岁呢，赶紧来看欧美方脸女明星是咋变方脸为神奇的吧！","url":"http://jingyan.baidu.com/article/c35dbcb02a630d8916fcbc98.html"},{"img":null,"title":"方脸女神支招最美修颜发型","desc":"纵观娱乐圈，女明星们似乎都是一致的小脸美女，要给大家看看娱乐圈那些方脸美女，她们一样很惊艳！","url":"http://lady.people.com.cn/n/2015/0804/c1014-27405655.html"}]}
             * wear : {"title":"服饰搭配","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RXWZCAI0ksb9UVwBCZ3hkXGucRq0Csb9UZw0kCS0UAqhmVqB1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRBWg.jpg","title":"女生方脸适合什么帽子","desc":"方脸型适合什么帽子？今天和大家一起看看方脸适合什么帽子吧。世界上有千千万万种脸型，每一种脸型有它自己独特的美，也有它自己独特的好处，那么假如你是方形脸，又想要带帽子的话，那么这时候可能会有点迷茫不知道","url":"http://dress.yxlady.com/201504/707591.shtml"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6I-WBRPGBUkS9UAL0UV30UgM0cSihRha9VVahVlGBVZqBmjL0dRenFvkrUgquWgquQXRfFSvrUCRBm8wuUhLuWV.jpg","title":"网购衣服党必看秘籍之宽脸/方脸/国字脸如何选购衣服","desc":"\"在生活当中，每个平凡的女生都是女神的潜力股，只是因为不恰当的装扮而掩盖自己的美丽。脸界霸王龙\u2014\u2014存在感超强的的宽脸、国字脸。该怎样穿才能使自己的腮帮子不那么明显。\"","url":"https://m.douban.com/note/518637096/"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZqBUkwZIsa9VZbuUV3BWkRZQRSBI8b9UX-hUgwZRAqhmgYZwRenFvkrUgquWgquQXRfFSvrUCRBm8YZUkRZIZ.jpg","title":"方形脸挑墨镜技巧","desc":"方型脸不建议选择太圆的，太圆会觉得角很明显，但是又不能选择太方的，太方会凸显你的线条。最好选择大的、但角度有点圆弧的。方型脸的侧面也会有点宽，如果选太细的镜腿，会显得腮部更明显，要避免这点，可以选脚宽","url":"http://fashion.sina.com.cn/s/ce/2015-05-19/0811/doc-icczmvup1895478-p3.shtml"}]}
             * makeup : {"title":"化妆技巧","list":[{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ICYBmAa0kZI9UkIuWV3BmhLZQRSBC0Q9UBCBmXWZWAaBRARBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZIg.jpg","title":"女生方脸化妆技巧","desc":"脸小的美眉给人的感觉，总是精致，像精灵一样惹人喜欢，然而方脸的美眉们也不用羡慕别人拥有的精致的小脸蛋了，今天教你们学习方脸的化妆技巧，打造属于自己的小脸。","url":"http://wenku.baidu.com/link?url=4HLJyCgGyaVETo7jJc34wUAN89LbSMYiineDcbxM-9DkltO9j-d21fziuJwCrk7QxKaKjkgqjfiC7ojFmDqZiCX8pvisSVP6OtgUgppdKAO"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RBi0UjSZRP-9VZb0WC3BUVwBcRYBI-G9UnWZU8R0mn-hVX-u1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZWA.jpg","title":"舒淇方脸淡妆同样女神范","desc":"精致的锥子脸是定义美女的标准，轮廓分明的方脸虽然看起来骨架比锥子脸大，可是妆容得宜同样吸睛美丽，圈中不乏方脸女神，舒淇、赵薇、高圆圆等都是方脸女神的代表。","url":"http://lady.163.com/13/1021/11/9BN5TKKD00264MHO.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXGuUjLZWT-9U--Zm83hInWBtS-BkCR9UX-ZIiCBWP-0WkqZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZUA.jpg","title":"怎样化方脸彩妆","desc":"很多MM都认为方脸化彩妆很难看，其实不然，看了米粒彩妆达人的《教你如何化彩妆之方脸彩妆》后，你就能轻松的搞定化方脸彩妆，\u201c妆\u201d出属于自己的美丽。","url":"http://jingyan.baidu.com/album/ab69b270e9d4642ca7189fc6.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXihmPihkZL9U8q0UV3uUn-ucRMBkhb9VPiZIiGhmsYhIgIBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZmh.jpg","title":"方脸如何化妆","desc":"方脸的人我们应该是见过很多的，那么给我印象最大的就是脸很方正，而且方脸的人看起来脸是很大的，如果我们从正面看的话，总是觉的方面并不是那么的好看的，因此对于我们方脸的人来，要如何化妆才好呢？","url":"http://jingyan.baidu.com/article/15622f24485b28fdfdbea57f.html"}]}
             * hair : []
             * organs : [[{"name":"标准眉","faceInfo":{"faceOrganInfo":{"title":"眉毛简介","name":"眉毛","desc":"又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]},"faceLevel":91,"faceFortune":83},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=brow&brow=æ \u0087å\u0087\u0086ç\u009c\u0089&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[119.70999908447,210.81500244141],"y":210.81500244141},{"name":"圆眼","faceInfo":{"faceOrganInfo":{"title":"眼形简介","name":"眼形","desc":"又称铜铃眼，眼睛圆的像铜铃，眼角边缘呈圆弧形，是最具福气的眼型了，大大圆圆的眼睛似乎里面藏了小精灵，聪明机灵、颜值爆棚。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e9939ce99383e79cbc.b98c60ac.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"三分钟就能搞定的日常眼妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232922?from=baidu&recommend=1","duration":"2:22","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1049.jpg","desc":"这个眼妆很日常，大约三分钟就能搞定喔"},{"imgType":"video","title":"單眼皮的基本妝容分享","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231650?from=baidu&recommend=1","duration":"5:47","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1051.jpg","desc":"分享单眼皮的基本妆容分享"},{"imgType":"video","title":"周末眼妆look\u2014\u2014红棕色的初秋","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227421?from=baidu&recommend=1","duration":"2:3","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/9695F8C75E4B249C.jpg-big2x.jpg ","desc":"日常眼妆分享"},{"imgType":"image","title":"火爆ins的亮片妆容","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180737?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1023.jpg","desc":"谁还不是个小仙女儿～现在超火爆的亮片妆容再搭配上双丸子头，可爱哟～"},{"imgType":"image","title":"你画出自己想要的眼影效果了吗？","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/176052?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502875845322c293dcb-small3x.jpg","desc":"给宝贝们分享一个眼影的画法"}]},"faceLevel":92,"faceFortune":96},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=eye&eye=é\u0093\u009cé\u0093\u0083ç\u009c¼&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[246.69499969482,236.5],"y":236.5}],[{"name":"标准鼻","faceInfo":{"faceOrganInfo":{"title":"鼻形简介","name":"鼻形","desc":"又称希腊鼻，此种鼻形鼻梁挺直、鼻头尖圆、鼻梁长度与鼻翼宽度的比例适当，百搭任何脸型，给人感觉高雅有气质、福气与颜值同在。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5b88ce8858ae9bcbb.d95051d6.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"鼻影大法好|两种不同产品的画法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/224191?from=baidu&recommend=1","duration":"1:49","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1072.jpg","desc":"和塌鼻子抗争多年的我终于研究出一套适合自己的画法希望可以帮助到你们！"},{"imgType":"video","title":"掌握鼻影修容术｜拯救烂大街的假鼻子","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/222378?from=baidu&recommend=1","duration":"2:52","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1FA01B8B44F8E457-big2x.jpg ","desc":"生活里大家的鼻子总归有不完美的地方，所以这节课我们就来讲鼻影啦！"},{"imgType":"video","title":"不打玻尿酸也有高鼻梁？刷对鼻影就好！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/221613?from=baidu&recommend=1","duration":"4:46","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1020.jpg","desc":"希望大家都可以学会，毕竟我觉得鼻子太重要了，谁让我没有高鼻梁呢！"},{"imgType":"video","title":"塌鼻子蒜头鼻怎么破？鼻影美图帮你忙","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/216075?from=baidu&recommend=1","duration":"4:37","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/FE06625A34D31801-big2x.jpg ","desc":"跟大家分享一下我自己平时是怎么画鼻子的，鼻影再修饰轮廓脸型来说是很重要的！ "}]},"faceLevel":91,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=nose&nose=å¸\u008cè\u0085\u008aé¼»&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[186.97999572754,298.13000488281],"y":298.13000488281},{"name":"白皙色","faceInfo":{"faceOrganInfo":{"title":"肤色简介","name":"肤色","desc":"又称亮白色，皮肤白皙细腻，闪耀亮白，给人一尘不染的感觉，是所有人都羡慕的肤色，不但颜值很高，就连化妆都能省去很多步骤。","imgUrl":"https://mms-graph.cdn.bcebos.com/face/faceOrgan/baixise.png"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"image","title":"包学包会的修容课堂 脸大也不怕了！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/161393?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1027.jpg","desc":"修容真正的意义在于，让轮廓更明显，五官更加立体"},{"imgType":"video","title":"【Rainie】第一次肤色修正？惊悚慎点哈","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/209573?from=baidu&recommend=1","duration":"7:11","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1021.jpg","desc":"不管你是黄皮白皮还是黑皮，肤色修正都相当重要！"},{"imgType":"video","title":"如何调整面部肤色不均","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/129454?from=baidu&recommend=1","duration":"2:18","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1022.jpg","desc":"如何调整面部肤色不均 ，这里有你必须了解的东西"},{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "}]},"faceLevel":97,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=skin_color&skin_color=äº®ç\u0099½è\u0089²_226_177_162&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[240.83999633789,324.82499694824],"y":324.82499694824}],[{"name":"瓜子脸","faceInfo":{"faceOrganInfo":{"title":"脸型简介","name":"脸型","desc":"又称心形脸，轮廓接近T形，下巴前翘明显。脸型标致，颜值爆表，此脸型的人善于创新，易得贵人赏识，所以是福气也很棒哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e5bf83e5bda2e884b8.730636d5.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"圆脸也适用的超自然日常妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/192366?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/DB7EF5AC725F690E.jpg-small3x.jpg","desc":"分享我觉得好看实用的技巧和妆容"},{"imgType":"video","title":"如何把大方脸化成欧式混血妆！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/230737?from=baidu&recommend=1","duration":"4:35","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1037.jpg ","desc":"们的模特是方脸妹子 一个妆容就能拯救"},{"imgType":"image","title":"磨骨太痛，来学修容吧。","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/188108?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1028.jpg","desc":"各种脸型俱全的新手指南"},{"imgType":"video","title":"不用整容，大饼脸变小V脸！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/227098?from=baidu&recommend=1","duration":"3:25","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/B5865F8726F97EBC-big2x.jpg ","desc":"什么，利用化妆修容可以改变脸型？"},{"imgType":"video","title":"四种不同脸型整容级修容法","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/223793?from=baidu&recommend=1","duration":"3:10","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1502288737103_Ji7HMZEW4D.png-big2x.jpg ","desc":"修容这个步骤不仅仅是变小脸这么简单，而是重塑五官的轮廓，让它看起来更加立体更自然"}]},"faceLevel":98,"faceFortune":94},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=face_shape&face_shape=ç\u0093\u009cå­\u0090è\u0084¸&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[175.4700012207,363.97999572754],"y":363.97999572754},{"name":"仰月唇","faceInfo":{"faceOrganInfo":{"title":"唇形简介","name":"唇形","desc":"又称弓口唇，上唇如弯弓一样棱角分明，嘴角上翘而饱满，是很有福气的唇形了，给人感觉气质性感而有个性，因此为颜值加分不少。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5bc93e58fa3e59487.c5a4a984.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"video","title":"复古风 气质红唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232999?from=baidu&recommend=1","duration":"4:58","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1106.jpg","desc":"拯救单眼皮系列，复古感的气质红唇妆容！"},{"imgType":"video","title":"口红涂抹挑战，做百变女魔王","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232912?from=baidu&recommend=1","duration":"3:8","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1096.jpg","desc":"口红涂抹千姿百态！绽放不同的色彩效果～薄涂厚涂咬唇都在大小不一样的自己"},{"imgType":"video","title":"3种基础唇妆，帮你颜值飙升","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232477?from=baidu&recommend=1","duration":"1:59","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1098.jpg","desc":"画好唇妆，飙升的不仅仅是颜值，还有气质！"},{"imgType":"video","title":"红丝绒唇妆","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231746?from=baidu&recommend=1","duration":"1:13","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1091.jpg","desc":"暖暖红丝绒唇妆，超有质感"},{"imgType":"video","title":"教你化个布灵布灵的时尚玻璃唇！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/231532?from=baidu&recommend=1","duration":"1:45","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1092.jpg","desc":"咬唇妆out了！教你化个布灵布灵的时尚玻璃唇！"}]},"faceLevel":92,"faceFortune":92},"url":"https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=mouth&mouth=å¼\u0093å\u008f£å\u0094\u0087&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1","loc":[188.97499847412,334.96000671387],"y":334.96000671387}]]
             */

            private DetectBean detect;
            private String faceShape;
            private int shapeId;
            private int gender;
            private int age;
            private double beauty;
            private FaceShapeInfoBean faceShapeInfo;
            private double yaw;
            private double roll;
            private double pitch;
            private double faceAreaRatio;
            private int halfBaked;
            private int glass;
            private SimiStarBean simiStar;
            private LuckBean luck;
            private StarBean star;
            private HairstyleBean hairstyle;
            private WearBean wear;
            private MakeupBean makeup;
            private List<List<Integer>> originDetect;
            private List<List<Integer>> largeDetect;
            private List<List<Integer>> xhcOriginDetect;
            private List<Integer> rect;
            private List<String> faceSummary;
            private List<?> hair;
            private List<List<OrgansBean>> organs;

            @NoArgsConstructor
            @Data
            public static class DetectBean {
                /**
                 * top : 205
                 * height : 181
                 * left : 98
                 * width : 180
                 */

                private int top;
                private int height;
                private int left;
                private int width;
            }

            @NoArgsConstructor
            @Data
            public static class FaceShapeInfoBean {
                /**
                 * title : 方形脸
                 * desc : 又称国字脸，此种脸型轮廓分明，下巴线条趋缓较平。此类脸型的女性意志坚强，有敏锐观察力，工作热诚有责任心
                 * url : https://www.baidu.com/s?ie=UTF-8&wd=æ¹å½¢è¸
                 * searchUrl : https://www.baidu.com/s?ie=UTF-8&wd=æ¹å½¢è¸
                 * img : https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e5a5b3-e696b9e5bda2e884b8.22596fef.jpg
                 * shapeTypes : [{"name":"脸型运势","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#luck"},{"name":"发型搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#hairstyle"},{"name":"服装搭配","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#wear"},{"name":"化妆技巧","url":"http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#makeup"}]
                 * othertypes : [{"name":"圆形脸","url":"http://graph.baidu.com/view/face?shapeId=1&gender=0"},{"name":"梨形脸","url":"http://graph.baidu.com/view/face?shapeId=2&gender=0"},{"name":"鹅蛋脸","url":"http://graph.baidu.com/view/face?shapeId=3&gender=0"},{"name":"瓜子脸","url":"http://graph.baidu.com/view/face?shapeId=4&gender=0"}]
                 */

                private String title;
                private String desc;
                private String url;
                private String searchUrl;
                private String img;
                private List<ShapeTypesBean> shapeTypes;
                private List<OthertypesBean> othertypes;

                @NoArgsConstructor
                @Data
                public static class ShapeTypesBean {
                    /**
                     * name : 脸型运势
                     * url : http://graph.baidu.com/view/face?shapeId=0&gender=0&sign=226d73b7e1cf86e0cfcf601600396176#luck
                     */

                    private String name;
                    private String url;
                }

                @NoArgsConstructor
                @Data
                public static class OthertypesBean {
                    /**
                     * name : 圆形脸
                     * url : http://graph.baidu.com/view/face?shapeId=1&gender=0
                     */

                    private String name;
                    private String url;
                }
            }

            @NoArgsConstructor
            @Data
            public static class SimiStarBean {
                private List<?> starList;
            }

            @NoArgsConstructor
            @Data
            public static class LuckBean {
                /**
                 * title : 脸型运势
                 * url : https://www.baidu.com/s?ie=UTF-8&wd=æ¹å½¢è¸è¸åè¿å¿
                 * desc : 方脸女性有较强的信服力和管理能力，在事业上有晋升运势，往往会取得较大成就；在感情上，容易遭遇桃花劫，要看准真感情，通常情感婚姻收获较大的月份是阴历8月和12月
                 */

                private String title;
                private String url;
                private String desc;
            }

            @NoArgsConstructor
            @Data
            public static class StarBean {
                /**
                 * title : 同脸型明星
                 * list : [{"name":"赵薇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITCBWAYZInC9UsRZmA3BCPCZtRbhIhq9UPG0kVMZWBCBUnCBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBIC.jpg","url":"http://graph.baidu.com/wordsearch?keyword=èµµè\u0096\u0087"},{"name":"李宇春","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IAI0UiGBmBW9UTGBCC3ZIsI0QRLZRPi9UZL0mlCZkCa0CPGZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVquWh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=æ\u009d\u008eå®\u0087æ\u0098¥"},{"name":"舒淇","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ITQ0UVahInQ9U0CZRA3BIBGZaRSZI8Y9UCw0mkM0U8MBWniZpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqZIh.jpg","url":"http://graph.baidu.com/wordsearch?keyword=è\u0088\u0092æ·\u0087"},{"name":"孙俪","img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZRhkZbuU0i9UBGuU83hIiWBcRIhIii9VhaBWsRZUPQBUXCu1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBmj.jpg","url":"http://graph.baidu.com/wordsearch?keyword=å­\u0099ä¿ª"}]
                 */

                private String title;
                private List<ListBean> list;

                @NoArgsConstructor
                @Data
                public static class ListBean {
                    /**
                     * name : 赵薇
                     * img : http://boscdn.bpc.baidu.com/mms-res/fFhO6ITCBWAYZInC9UsRZmA3BCPCZtRbhIhq9UPG0kVMZWBCBUnCBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUVqBIC.jpg
                     * url : http://graph.baidu.com/wordsearch?keyword=èµµè
                     */

                    private String name;
                    private String img;
                    private String url;
                }
            }

            @NoArgsConstructor
            @Data
            public static class HairstyleBean {
                /**
                 * title : 发型搭配
                 * list : [{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6R-WhmnihIjb9UXQZRs30kBQBQRLBCCq9U0G0UjRhUkaZUjwZdRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgauUV.jpg","title":"方脸女生适合哪些发型","desc":"不同的脸型适合不同的发型，那对于方形脸的MM们该如何选择发型呢？方形脸只要巧妙的搭配发型，也能够让你脸型变得小巧可爱哦，下面就简单的介绍下关于方形脸适合的发型吧，希望能够帮助到你们。","url":"http://jingyan.baidu.com/article/ab0b5630cfedf2c15afa7d9d.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IjMBWkYuUsI9UnGuWC3ZCP-ucRLZkAq9UniuUswhUi-0CAS0dRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZUj.jpg","title":"方脸MM适合什么发型，7款发型任你选","desc":"方脸姑娘留什么发型最秀气减龄？看欧美女明星就行！腮帮子较宽，下巴不够尖的问题在各位欧美女明星那里完全不是问题，找对合适的发型，立刻让你的大方脸减龄十岁呢，赶紧来看欧美方脸女明星是咋变方脸为神奇的吧！","url":"http://jingyan.baidu.com/article/c35dbcb02a630d8916fcbc98.html"},{"img":null,"title":"方脸女神支招最美修颜发型","desc":"纵观娱乐圈，女明星们似乎都是一致的小脸美女，要给大家看看娱乐圈那些方脸美女，她们一样很惊艳！","url":"http://lady.people.com.cn/n/2015/0804/c1014-27405655.html"}]
                 */

                private String title;
                private List<ListBeanX> list;

                @NoArgsConstructor
                @Data
                public static class ListBeanX {
                    /**
                     * img : http://boscdn.bpc.baidu.com/mms-res/fFhO6R-WhmnihIjb9UXQZRs30kBQBQRLBCCq9U0G0UjRhUkaZUjwZdRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgauUV.jpg
                     * title : 方脸女生适合哪些发型
                     * desc : 不同的脸型适合不同的发型，那对于方形脸的MM们该如何选择发型呢？方形脸只要巧妙的搭配发型，也能够让你脸型变得小巧可爱哦，下面就简单的介绍下关于方形脸适合的发型吧，希望能够帮助到你们。
                     * url : http://jingyan.baidu.com/article/ab0b5630cfedf2c15afa7d9d.html
                     */

                    private String img;
                    private String title;
                    private String desc;
                    private String url;
                }
            }

            @NoArgsConstructor
            @Data
            public static class WearBean {
                /**
                 * title : 服饰搭配
                 * list : [{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RXWZCAI0ksb9UVwBCZ3hkXGucRq0Csb9UZw0kCS0UAqhmVqB1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRBWg.jpg","title":"女生方脸适合什么帽子","desc":"方脸型适合什么帽子？今天和大家一起看看方脸适合什么帽子吧。世界上有千千万万种脸型，每一种脸型有它自己独特的美，也有它自己独特的好处，那么假如你是方形脸，又想要带帽子的话，那么这时候可能会有点迷茫不知道","url":"http://dress.yxlady.com/201504/707591.shtml"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6I-WBRPGBUkS9UAL0UV30UgM0cSihRha9VVahVlGBVZqBmjL0dRenFvkrUgquWgquQXRfFSvrUCRBm8wuUhLuWV.jpg","title":"网购衣服党必看秘籍之宽脸/方脸/国字脸如何选购衣服","desc":"\"在生活当中，每个平凡的女生都是女神的潜力股，只是因为不恰当的装扮而掩盖自己的美丽。脸界霸王龙\u2014\u2014存在感超强的的宽脸、国字脸。该怎样穿才能使自己的腮帮子不那么明显。\"","url":"https://m.douban.com/note/518637096/"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RZqBUkwZIsa9VZbuUV3BWkRZQRSBI8b9UX-hUgwZRAqhmgYZwRenFvkrUgquWgquQXRfFSvrUCRBm8YZUkRZIZ.jpg","title":"方形脸挑墨镜技巧","desc":"方型脸不建议选择太圆的，太圆会觉得角很明显，但是又不能选择太方的，太方会凸显你的线条。最好选择大的、但角度有点圆弧的。方型脸的侧面也会有点宽，如果选太细的镜腿，会显得腮部更明显，要避免这点，可以选脚宽","url":"http://fashion.sina.com.cn/s/ce/2015-05-19/0811/doc-icczmvup1895478-p3.shtml"}]
                 */

                private String title;
                private List<ListBeanXX> list;

                @NoArgsConstructor
                @Data
                public static class ListBeanXX {
                    /**
                     * img : http://boscdn.bpc.baidu.com/mms-res/fFhO6RXWZCAI0ksb9UVwBCZ3hkXGucRq0Csb9UZw0kCS0UAqhmVqB1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRBWg.jpg
                     * title : 女生方脸适合什么帽子
                     * desc : 方脸型适合什么帽子？今天和大家一起看看方脸适合什么帽子吧。世界上有千千万万种脸型，每一种脸型有它自己独特的美，也有它自己独特的好处，那么假如你是方形脸，又想要带帽子的话，那么这时候可能会有点迷茫不知道
                     * url : http://dress.yxlady.com/201504/707591.shtml
                     */

                    private String img;
                    private String title;
                    private String desc;
                    private String url;
                }
            }

            @NoArgsConstructor
            @Data
            public static class MakeupBean {
                /**
                 * title : 化妆技巧
                 * list : [{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6ICYBmAa0kZI9UkIuWV3BmhLZQRSBC0Q9UBCBmXWZWAaBRARBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZIg.jpg","title":"女生方脸化妆技巧","desc":"脸小的美眉给人的感觉，总是精致，像精灵一样惹人喜欢，然而方脸的美眉们也不用羡慕别人拥有的精致的小脸蛋了，今天教你们学习方脸的化妆技巧，打造属于自己的小脸。","url":"http://wenku.baidu.com/link?url=4HLJyCgGyaVETo7jJc34wUAN89LbSMYiineDcbxM-9DkltO9j-d21fziuJwCrk7QxKaKjkgqjfiC7ojFmDqZiCX8pvisSVP6OtgUgppdKAO"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6RBi0UjSZRP-9VZb0WC3BUVwBcRYBI-G9UnWZU8R0mn-hVX-u1RenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZWA.jpg","title":"舒淇方脸淡妆同样女神范","desc":"精致的锥子脸是定义美女的标准，轮廓分明的方脸虽然看起来骨架比锥子脸大，可是妆容得宜同样吸睛美丽，圈中不乏方脸女神，舒淇、赵薇、高圆圆等都是方脸女神的代表。","url":"http://lady.163.com/13/1021/11/9BN5TKKD00264MHO.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXGuUjLZWT-9U--Zm83hInWBtS-BkCR9UX-ZIiCBWP-0WkqZwRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZUA.jpg","title":"怎样化方脸彩妆","desc":"很多MM都认为方脸化彩妆很难看，其实不然，看了米粒彩妆达人的《教你如何化彩妆之方脸彩妆》后，你就能轻松的搞定化方脸彩妆，\u201c妆\u201d出属于自己的美丽。","url":"http://jingyan.baidu.com/album/ab69b270e9d4642ca7189fc6.html"},{"img":"http://boscdn.bpc.baidu.com/mms-res/fFhO6IXihmPihkZL9U8q0UV3uUn-ucRMBkhb9VPiZIiGhmsYhIgIBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgRZmh.jpg","title":"方脸如何化妆","desc":"方脸的人我们应该是见过很多的，那么给我印象最大的就是脸很方正，而且方脸的人看起来脸是很大的，如果我们从正面看的话，总是觉的方面并不是那么的好看的，因此对于我们方脸的人来，要如何化妆才好呢？","url":"http://jingyan.baidu.com/article/15622f24485b28fdfdbea57f.html"}]
                 */

                private String title;
                private List<ListBeanXXX> list;

                @NoArgsConstructor
                @Data
                public static class ListBeanXXX {
                    /**
                     * img : http://boscdn.bpc.baidu.com/mms-res/fFhO6ICYBmAa0kZI9UkIuWV3BmhLZQRSBC0Q9UBCBmXWZWAaBRARBpRenFvkrUgquWgquQXRfFSvrUCRBm8bZUgIZIg.jpg
                     * title : 女生方脸化妆技巧
                     * desc : 脸小的美眉给人的感觉，总是精致，像精灵一样惹人喜欢，然而方脸的美眉们也不用羡慕别人拥有的精致的小脸蛋了，今天教你们学习方脸的化妆技巧，打造属于自己的小脸。
                     * url : http://wenku.baidu.com/link?url=4HLJyCgGyaVETo7jJc34wUAN89LbSMYiineDcbxM-9DkltO9j-d21fziuJwCrk7QxKaKjkgqjfiC7ojFmDqZiCX8pvisSVP6OtgUgppdKAO
                     */

                    private String img;
                    private String title;
                    private String desc;
                    private String url;
                }
            }

            @NoArgsConstructor
            @Data
            public static class OrgansBean {
                /**
                 * name : 标准眉
                 * faceInfo : {"faceOrganInfo":{"title":"眉毛简介","name":"眉毛","desc":"又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg"},"markupInfo":{"title":"妆容技巧","content":[{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]},"faceLevel":91,"faceFortune":83}
                 * url : https://graph.baidu.com/view/faceorgan?sign=226d73b7e1cf86e0cfcf601600396176&index=0&organType=brow&brow=æ åç&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=&isbface=1
                 * loc : [119.70999908447,210.81500244141]
                 * y : 210.81500244141
                 */

                private String name;
                private FaceInfoBean faceInfo;
                private String url;
                private double y;
                private List<Double> loc;

                @NoArgsConstructor
                @Data
                public static class FaceInfoBean {
                    /**
                     * faceOrganInfo : {"title":"眉毛简介","name":"眉毛","desc":"又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。","imgUrl":"https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg"}
                     * markupInfo : {"title":"妆容技巧","content":[{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]}
                     * faceLevel : 91
                     * faceFortune : 83
                     */

                    private FaceOrganInfoBean faceOrganInfo;
                    private MarkupInfoBean markupInfo;
                    private int faceLevel;
                    private int faceFortune;

                    @NoArgsConstructor
                    @Data
                    public static class FaceOrganInfoBean {
                        /**
                         * title : 眉毛简介
                         * name : 眉毛
                         * desc : 又称自然眉，眉头到眉尾在同一水平线上，是标准东方人的脸部特征，福气不错，且此眉形的人气质大方、自然，会给颜值加分哦。
                         * imgUrl : https://mms-res.cdn.bcebos.com/graph/face/faceOrgan/e6a087e58786e79c89.497dc3fa.jpg
                         */

                        private String title;
                        private String name;
                        private String desc;
                        private String imgUrl;
                    }

                    @NoArgsConstructor
                    @Data
                    public static class MarkupInfoBean {
                        /**
                         * title : 妆容技巧
                         * content : [{"imgType":"image","title":"眉毛总画不好？这样做，省下3001块纹眉钱！（二）","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg","desc":"唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。 "},{"imgType":"image","title":"史上最详细的画眉技巧","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/180013?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/56407424E31C777A.jpg-small3x.jpg","desc":"小白画眉，我把我能想到的都把图做出来了"},{"imgType":"image","title":"植村秀砍刀眉笔+画眉教程","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/171914?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/39.jpg","desc":"黄金眉形"},{"imgType":"image","title":"眉毛画得生硬不自然？那是踩了画眉误区","moreUrl":"https://static.xiaohongchun.com/xhc-ai/sharebuy/195167?from=baidu&recommend=1","duration":"","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/41.jpg","desc":"画个自然的眉型能够让五官显得更加立体，更有魅力。"},{"imgType":"video","title":"防手残！教你画出完美对称的眉毛！","moreUrl":"https://static.xiaohongchun.com/xhc-ai/video/232170?from=baidu&recommend=1","duration":"1:17","imgUrl":"https://mms-graph.cdn.bcebos.com/face/xhc/1059.jpg","desc":"手残党画眉毛的时候，一不小心就一高一低，或者总是不对称！炒鸡尴尬。"}]
                         */

                        private String title;
                        private List<ContentBean> content;

                        @NoArgsConstructor
                        @Data
                        public static class ContentBean {
                            /**
                             * imgType : image
                             * title : 眉毛总画不好？这样做，省下3001块纹眉钱！（二）
                             * moreUrl : https://static.xiaohongchun.com/xhc-ai/sharebuy/184670?from=baidu&recommend=1
                             * duration :
                             * imgUrl : https://mms-graph.cdn.bcebos.com/face/xhc/48.jpg
                             * desc : 唇唇提醒大家眉形一定要选对， 新手不要要求太多眉形，先从标准眉开始。
                             */

                            private String imgType;
                            private String title;
                            private String moreUrl;
                            private String duration;
                            private String imgUrl;
                            private String desc;
                        }
                    }
                }
            }
        }
    }
}

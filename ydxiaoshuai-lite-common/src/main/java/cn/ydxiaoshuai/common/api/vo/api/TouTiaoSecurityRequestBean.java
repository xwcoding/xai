package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className TouTiaoSecurityRequestBean
 * @Description 安全监测请求对象
 * @Date 2020/6/17-16:49
 **/
@NoArgsConstructor
@Data
public class TouTiaoSecurityRequestBean {

    private List<String> targets;
    private List<TasksBean> tasks;

    @NoArgsConstructor
    @Data
    public static class TasksBean {

        private String image;
        private String image_data;
    }
}

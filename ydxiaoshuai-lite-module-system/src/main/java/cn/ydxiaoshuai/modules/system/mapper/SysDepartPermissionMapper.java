package cn.ydxiaoshuai.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.system.entity.SysDepartPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 部门权限表
 * @Author: 小帅丶
 * @Date:   2020-02-11
 * @Version: V1.0
 */
public interface SysDepartPermissionMapper extends BaseMapper<SysDepartPermission> {

}

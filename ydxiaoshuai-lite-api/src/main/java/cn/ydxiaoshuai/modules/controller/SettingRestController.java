package cn.ydxiaoshuai.modules.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.DateUtils;
import cn.ydxiaoshuai.modules.liteuser.entity.LiteUserInfo;
import cn.ydxiaoshuai.modules.liteuser.service.ILiteUserInfoService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author 小帅丶
 * @className LiteUserRestController
 * @Description 一些系统接口
 * @Date 2020/9/16-11:38
 **/
@Controller
@RequestMapping(value="/rest/set")
@Scope("prototype")
@Api(tags = {"首页轮播、刷新文案API"})
@Slf4j
public class SettingRestController extends ApiRestController {
    @Autowired
    private ILiteUserInfoService liteUserInfoService;
    /**
     * @Author 小帅丶
     * @Description 用户天数更新
     * @Date  2020/2/24 18:09
     * @return void
     **/
    @RequestMapping(value = "/user/update_days",method = {RequestMethod.GET})
    public ResponseEntity<Object> updateDays(){
        log.info("=======访问的IP"+request.getRemoteAddr()+"======访问的User-Agent:"+userAgent);
        BaseResponseBean bean = new BaseResponseBean();
        try {
            startTime = System.currentTimeMillis();
            liteUserInfoService.updateUserDays();
            bean.setCode(200);
            bean.setMessage("更新成功");
        } catch (Exception e) {
            log.info("中草药识别接口出错了"+e.getMessage());
            bean.setCode(500);
            bean.setMessage("系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming);
        beanStr = JSON.toJSONString(bean);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
}

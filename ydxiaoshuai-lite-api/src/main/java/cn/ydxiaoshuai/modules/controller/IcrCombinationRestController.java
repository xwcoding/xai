package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.sdkpro.AipImageClassifyPro;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


/**
 * @Description 图像识别组合接口
 * @author 小帅丶
 * @className IcrCombinationRestController
 * @Date 2020/7/17-10:11
 **/
@Controller
@RequestMapping(value = "/rest/combination")
@Scope("prototype")
@Slf4j
@Api(tags = "图像识别组合接口-API")
public class IcrCombinationRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    AipImageClassifyPro aipImageClassifyPro = BDFactory.getAipImageClassifyPro();
    /**
     * @Description 组合图像识别接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年7月17日
     **/
    @RequestMapping(value = "/detect", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> detectCombination(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        String combination = "";
        try {
            startTime = System.currentTimeMillis();
            String imgBase64 = Base64Util.encode(file.getBytes());
            combination = aipImageClassifyPro.combinationStr(imgBase64);
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("垃圾分类查询接口出错了" + errorMsg);
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming,combination);
        //响应的内容
        return new ResponseEntity<Object>(combination, httpHeaders, HttpStatus.OK);
    }
}

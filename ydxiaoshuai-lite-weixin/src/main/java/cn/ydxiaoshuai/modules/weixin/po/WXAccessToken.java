package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description 微信AccessToken对象
 * @author 小帅丶
 * @className WXAccessToken
 * @Date 2019/11/28-10:42
 **/
@Data
public class WXAccessToken {
    /** 获取到的凭证 */
    private String access_token;
    /** 凭证有效时间，单位：秒  默认2小时 7200s*/
    private String expires_in;
    /** 错误码 */
    private Integer errcode;
    /** 错误信息 */
    private String errmsg;
}
